<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeC;
use App\Http\Controllers\AboutC;
use App\Http\Controllers\PortfolioC;
use App\Http\Controllers\ContactC;


Route::get('/', [HomeC::class, 'index']);

Route::get('/about', [AboutC::class, 'index']);

Route::get('/portfolio', [PortfolioC::class, 'index']);

Route::get('/contact', [ContactC::class, 'index']);