@extends('layouts.main')

@section('content')
<section class="container-fluid main-container container-home p-0 revealator-slideup revealator-once revealator-delay1">
    <div class="color-block d-none d-lg-block"></div>
    <div class="row home-details-container align-items-center">
        <div class="col-lg-4 bg position-fixed d-none d-lg-block"></div>
        <div class="col-12 col-lg-8 offset-lg-4 home-details text-left text-sm-center text-lg-left">
            <div>
                <img src="img/IMG_20191121_180715_501.jpg" class="img-fluid main-img-mobile d-none d-sm-block d-lg-none" alt="my picture" />
                <h6 class="text-uppercase open-sans-font mb-0 d-block d-sm-none d-lg-block">Hai There!</h6>
                <h1 class="text-uppercase poppins-font"><span>I'm</span> Kerisna</h1>
                <p class="open-sans-font">I am a student from the Informatics Engineering Education Study Program, Informatics Engineering Department, Faculty of Engineering and Vocational, Uiversitas Pendidikan Ganesha, Bali.</p>
                <a href="about" class="btn btn-about">more about me</a>
            </div>
        </div>
    </div>
</section>
@endsection