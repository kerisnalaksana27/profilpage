<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PortfolioC extends Controller
{
    public function index()
    {
        return view('portfolio', [
        "title" => "Portfolio",
        "class" => "portfolio"
    ]);
    }//
}
